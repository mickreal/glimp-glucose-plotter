import pandas as pd
import csv 
import sys
from matplotlib import pyplot as plt
from matplotlib import dates as pltd
import matplotlib.patches as patches
from datetime import datetime
import numpy as np


file_data = open('GlicemiaMisurazioni.csv', 'r', encoding='utf16')
data = csv.reader(file_data, delimiter=';')

dates = dict()

for row in data:
    full_date = datetime.strptime(row[1],"%d/%m/%Y %H.%M.%S")
    d = {
        "date" : full_date,
        "value": float(row[4]),
        "val2" : float(row[5])
    }
    date = full_date.strftime("%Y%m%d")
    if date not in dates.keys():
        dates[date]={
                    "time" : [],
                    "value": [],
                    "val2": []
                }
    dates[date]['time'].append(d['date'])
    dates[date]['value'].append(d['value'])
    dates[date]['val2'].append(d['val2'])

for date in dates:
    ticks = np.array(dates[date]['time'])
    values = np.array(dates[date]['value'])
    min_tick = np.amin(ticks)
    max_tick = np.amax(ticks)
    top=140
    bottom=70

    upper = np.ma.masked_where(values < top, values)
    lower = np.ma.masked_where(values > bottom, values)
    hours = pltd.HourLocator()
    dfmt = pltd.DateFormatter('%H:%M')

    fig = plt.figure(figsize=(15,7))
    ax = fig.add_subplot(111)
    ax.xaxis.set_major_locator(hours)
    ax.xaxis.set_major_formatter(dfmt)
    ax.plot(ticks,upper,linewidth=2, color="red", zorder=2)
    ax.plot(ticks,lower,linewidth=2, color="red",zorder=3)
    ax.plot(ticks,values,linewidth=2, color="green", zorder=1)
    ax.add_patch(
        patches.Rectangle(
            (min_tick, bottom),   # (x,y)
            max_tick,          # width
            top-bottom,          # height,
            alpha=0.1,
            facecolor="#333333"
        )
    )
    ax.tick_params(axis='x', rotation=70)
    ax.set_ylim(50,300)
    ax.set_xlim(min_tick,max_tick)
    ax.grid()
    ax.set_title(date, fontdict={
        'fontsize' : 20
    })
    fig.savefig(date+'.png', dpi=100)
    plt.gcf().clear()
