Glimp-glucose-plotter


###What does it do
this program plots daily charts for the glucose data from glimp app exported to the dropbox folder
###How To run it
- download git repo
- open dropbox folder with glimp data
- extract "GlicemiaMisurazioni.csv.gz" into the plotter folder
- install python
- add python to executables
- browse via command line to the main.py
- run "pip install requirements.txt"
- run "py main.py"
###Is it final?
Hopefully no, i will try to add more features 
